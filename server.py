#!/usr/bin/python3

import socket
import selectors
import threading

from terminal import TelnetTerminal, TerminalSettings
from service import TelnetService


class TelnetServer:
    host = ''
    port: int = 23
    selector: selectors.DefaultSelector = None
    terminals: [TelnetTerminal] = []
    listenSocket = None
    thread = None
    service: TelnetService = None

    def __init__(self, host='', port=23, service=None, take_control=False):
        """By default creates a Telnet server listening on all addresses, port 23, with the default client handler
        (that just prints a banner and exits)"""

        self.host = host
        self.port = port
        self.selector = selectors.DefaultSelector()

        if service is None:
            self.service = TelnetService()
        else:
            self.service = service

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((self.host, self.port))
        s.listen()
        s.setblocking(False)
        self.selector.register(s, selectors.EVENT_READ, data=None)
        self.listenSocket = s

        if take_control:
            print("Telnet server is main thread")
            self.thread = threading.current_thread()
            self.handle_socket_events()
        else:
            print("Telnet server is starting as child thread")
            self.thread = threading.Thread(target=self.handle_socket_events)
            self.thread.start()
            return

    def die(self):
        """Kills the server and all releases all its resources."""
        print("terminating Telnet server")
        if self.listenSocket is not None:
            self.listenSocket.close()
            self.listenSocket = None
        for terminal in self.terminals:
            terminal.close()

    def handle_socket_events(self):
        self.__handle_socket_events()
        self.die()
        print("Telnet server terminated")
        return

    def __handle_socket_events(self):
        try:
            while True:
                events = self.selector.select(timeout=None)  # blocks
                for key, mask in events:
                    if isinstance(key.data, TelnetTerminal):
                        self.service_terminal(key, mask)
                    else:
                        self.accept_wrapper(key.fileobj)
        except KeyboardInterrupt:
            return

    def accept_wrapper(self, sock: socket):
        conn, addr = sock.accept()
        conn.setblocking(False)

        # put all TCP URG data in the front of the stream
        conn.setsockopt(socket.SOL_SOCKET, socket.SO_OOBINLINE, 1)

        terminal_settings = TerminalSettings()
        terminal = TelnetTerminal(terminal_settings, conn, self.selector)
        self.service.initHandshake(terminal=terminal)
        self.selector.register(conn, selectors.EVENT_READ, data=terminal)

    def service_terminal(self, key, mask):
        sock = key.fileobj
        terminal: TelnetTerminal = key.data
        if mask & selectors.EVENT_READ:
            print("reading from client")
            data = sock.recv(1024)
            if data:
                self.service.handleInput(data=data, terminal=terminal)
            else:
                print("client closed socket")
                self.selector.unregister(sock)
                sock.close()
        if mask & selectors.EVENT_WRITE:
            print("WRITE NOT SUPPORTED")
            return


if __name__ == "__main__":
    server = TelnetServer(port=9997, take_control=True)
