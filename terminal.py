import socket
import selectors

from protocol import Code, ESC


class TerminalSettings:
    allow_remote_binary = False
    is_remote_binary = False
    is_local_binary = False


clientId: int = 0


def get_next_client_id() -> int:
    global clientId
    id = clientId
    clientId = clientId + 1
    return id


class TelnetTerminal:
    """Implements the 'Network Virtual Terminal' (NVT) as described in RFC854"""
    encoding: str = "utf8"
    socket: socket = None
    selector: selectors.DefaultSelector
    outbuffer: bytearray
    clientId: int

    def __init__(self, settings: TerminalSettings, conn: socket, selector: selectors.DefaultSelector):
        self.settings = settings
        self.socket = conn
        self.selector = selector
        self.outbuffer = bytearray()
        self.clientId = get_next_client_id()

    def read(self) -> (str, bytes):
        """Reads the next couple of characters sent by the client."""
        msg = self.socket.recv(1024)
        # data = self.extract_and_parse_all_codes(msg)
        return msg.decode(self.encoding)

    def write(self, data=None, oob: bool = False) -> bool:
        """Writes string to the client in the encoding specified by TelnetTerminal.encoding. If oob = True then send the
         data Out Of Band."""

        if isinstance(data, str):
            data = bytearray(data, self.encoding)

        n: int
        if oob:
            if data is not None:
                n = self.socket.send(data, socket.MSG_OOB)
                if n != len(data):
                    return False
                return True
            else:
                raise ValueError('OOB data missing')
        elif data is None:  # meaning we should write the outbuffer
            # print("working on the outbuffer")
            if len(self.outbuffer) == 0:
                # print("outbuffer empty - events: read")
                self.selector.modify(self.socket, selectors.EVENT_READ, data=self)
                return True

            n = self.socket.send(self.outbuffer)
            if n == len(self.outbuffer):
                # print("emptied outbuffer")
                self.outbuffer = bytearray()
                return True
            else:  # outbuffer is not empty, schedule again
                # print("outbuffer contains more data, yet to be written")
                self.outbuffer = self.outbuffer[n:]
                self.selector.modify(self.socket, selectors.EVENT_READ | selectors.EVENT_WRITE, data=self)
                return True
        else:  # DEFAULT CASE
            # print("appending to outbuffer")
            self.outbuffer += data
            return self.write(data=None, oob=False)

    def close(self):
        """Terminates the terminal and disconnects the client."""
        print("Closing terminal for user ", clientId)
        self.selector.unregister(self.socket)
        self.socket.close()

    def extract_and_parse_all_codes(self, data: bytes) -> (bytes, [Code]):
        codes = self.find_all_codes(data)
        data_bytes: bytes = self.remove_all_codes(data, codes)
        return data_bytes, codes

    def find_all_codes(self, data: bytes) -> (int, [Code]):
        """Returns (un-parsed bytes, list of codes)"""
        codes: [Code] = []
        i: int = -1
        while True:
            n = data.find(ESC.IAC, i + 1)
            if n == -1:
                return 0, codes
            if n + 1 >= len(data):
                # unable to parse the last byte, since it belongs to a code that is outside of the data buffer
                return 1, codes
            if data[n + 1] == 0xFA:
                if len(data) >= n + 2:
                    # unable to parse the last two bytes, since they belong to a code that is outside of data buffer
                    return 2, codes
                else:
                    codes.append(Code(i, data[i:i + 2]))
            elif data[n + 1] | 0x0F == 0xFF:  # FF Fx     (and not FF FA, because we checked that earlier)
                codes.append(Code(i, data[i:i + 2]))  # so its a 2-byte code
            else:
                codes.append(Code(i, data[i:i + 2], False))  # unrecognized code

    def remove_all_codes(self, data: bytes, codes: [Code]) -> bytes:
        """Removes the Telnet codes, returns only the actual data"""
        if len(codes) == 0:
            return data

        datalen: int = len(data)
        for code in codes:
            if code.is_data_0xff():
                datalen -= 1
            else:
                datalen -= len(code.data)

        result = bytearray(datalen)
        data_index: int = 0
        data_end: int = 0
        prev_code_lens: int = 0
        prev_code_index: int = 0
        prev_code_len: int = 0

        for code in codes:
            if prev_code_index == 0:
                data_end = code.index
            else:
                data_index = data_end
                data_end = prev_code_index - prev_code_lens

            result[data_index: data_end] = data[(prev_code_index + prev_code_len): code.index]
            prev_code_lens += len(code.data)

            if code.is_data_0xff():
                result[data_end] = 0xff
                data_end += 1

        return result

    def set_local_binary_mode(self):
        self.socket.send("\xFF\xFA\x00")
