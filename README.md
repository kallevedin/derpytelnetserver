# Derpy Telnet Server #

DerpyTelnetServer is the derpy **Telnet server** written in **Python3**. 

It uses a single thread and select()s on nonblocking sockets.

It should be enough for writing simple telnet art projects and whatnot. 

### How to use this code ###
1. Copy the code
2. Extend or rewrite the TelnetService class with your own code.
3. Think for yourself.
4. Run the code.
5. ????
6. PROFIT

### Derpty Telnet Server has support for this: ###
* RFC854 and RFC855 (basic Telnet with support for TCP URG)
* Some basic ANSI escape codes 

Most Telnet clients does not support the Telnet protocol extension RFCs, and are essentially just netcat + ansi escape 
interpreters, so it is rather pointless to implement the more arcane protocol extensions (RFC856, RFC857, RFC858, 
RFC1073, RFC1091). I tried it, but both Linux and Windows "default" telnet client just ignores these commands.

The windows telnet client even hangs when it sees some of the Telnet escape sequences! **WTF**

### License ###
DerpyTelnetServer is published and released under [Creative Commons Zero](https://creativecommons.org/share-your-work/public-domain/cc0).

This means that you can do _whatever you want_ with this code. You can steal it and claim you wrote it. You can determine yourself how much of a fellow nice human being you are. (JUST LIKE IN REAL LIFE!!! OMG)

### Authors ###

* Kalle Vedin (kalle.vedin@fripost.org)