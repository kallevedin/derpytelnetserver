class ESC:
    # characters used in telnet
    NUL: bytes = b'\x00'  # NUL character
    BEL: bytes = b'\x07'
    BRK: bytes = b'\x81'  # special BREAK character

    # vocabulary used in RFCs
    FF: bytes = b'\xFF\xFF'  # read as \xFF (escaped \xFF)
    NOP: bytes = b'\xF1'  # IAC NOP
    DATA_MARK: bytes = b'\xF2'  # IAC DATA_MARK
    IAC: bytes = b'\xFF'  # should always be followed by something
    SB: bytes = b'\xFA'  # IAC SB <... sub negotioation sequence ... > IAC SE
    SE: bytes = b'\xF0'  # IAC SB <... sub negotioation sequence ... > IAC SE
    WILL: bytes = b'\xFA\xFB'  # IAC WILL
    WONT: bytes = b'\xFA\xFC'  # IAC WONT
    DO: bytes = b'\xFA\xFD'  # IAC DO
    DONT: bytes = b'\xFA\xFE'  # IAC DONT
    GO_AHEAD: bytes = b'\xF9'  # IAC GO_AHEAD

    BINARY: bytes = b'\x00'                     # RFC856 (send binary data)
    ECHO: bytes = b'\x01'                       # RFC857 (enable/disable echo)
    SUPPRESS_GO_AHEAD: bytes = b'\x03'          # RFC858 (used to disable an oldskool command that no one uses)
    TERMINAL_TYPE: bytes = b'24'                # RFC1091 (Telnet Terminal-Type Option)
    NAWS: bytes = b'31'                         # RFC1073 (Negotiate About Window Size)

    IAC_WILL_ECHO: bytes = IAC + WILL + ECHO
    IAC_WONT_ECHO: bytes = IAC + WONT + ECHO
    IAC_DO_ECHO: bytes = IAC + DO + ECHO
    IAC_DONT_ECHO: bytes = IAC + DONT + ECHO

    IAC_WILL_SUPRESS_GO_AHEAD: bytes = IAC + WILL + SUPPRESS_GO_AHEAD
    IAC_WONT_SUPPRESS_GO_AHEAD: bytes = IAC + WONT + SUPPRESS_GO_AHEAD
    IAC_DO_SUPPRESS_GO_AHEAD: bytes = IAC + DO + SUPPRESS_GO_AHEAD
    IAC_DONT_SUPPRESS_GO_AHEAD: bytes = IAC + DONT + SUPPRESS_GO_AHEAD

    IAC_WILL_NAWS: bytes = IAC + WILL + NAWS
    IAC_WONT_NAWS: bytes = IAC + WONT + NAWS
    IAC_DO_NAWS: bytes = IAC + DO + NAWS
    IAC_DONT_NAWS: bytes = IAC + DONT + NAWS

    # NEW_TERMINAL_SIZE <16-bit (BE) columns> <16-bit (BE) rows> END_TERMINAL_SIZE
    NEW_TERMINAL_SIZE: bytes = IAC + SB + NAWS
    END_TERMIAL_SIZE: bytes = IAC + SE

    # server: IAC_DO_TERMINAL_TYPE
    # client: IAC_WILL_TERMINAL_TYPE
    # server: REQUEST_TERMINAL_TYPE
    # client: sends its terminal type on the form specified by send_terminal_type
    IAC_WILL_TERMINAL_TYPE: bytes = IAC + WILL + TERMINAL_TYPE
    IAC_WONT_TERMINAL_TYPE: bytes = IAC + WONT + TERMINAL_TYPE
    IAC_DO_TERMINAL_TYPE: bytes = IAC + DO + TERMINAL_TYPE
    IAC_DONT_TERMINAL_TYPE: bytes = IAC + DONT + TERMINAL_TYPE
    SUBCOMMAND_REQUEST_TERMINAL_TYPE: bytes = b'\x01'
    SUBCOMMAND_THIS_TERMINAL_IS: bytes = b'\x00'

    REQUEST_TERMINAL_TYPE: bytes = IAC + SB + TERMINAL_TYPE + SUBCOMMAND_REQUEST_TERMINAL_TYPE + IAC + SE

    def send_terminal_type(tty: bytes) -> bytes:
        return ESC.IAC + ESC.SB + ESC.TERMINAL_TYPE + ESC.SUBCOMMAND_THIS_TERMINAL_IS + tty + ESC.IAC + ESC.SE


class ansi:
    # misc
    HOME: bytes = b"\x1b[H"                 # moves cursor to top left corner
    RESET: bytes = b"\x1bc"                 # resets the terminal settings and moves cursor to top left corner (we hope)

    # video
    NORMAL: bytes = b"\x1b[0m"
    INVERT: bytes = b"\x1b[7m"
    INVERT_OFF: bytes = b"\x1b[27m"
    BLINK: bytes = b"\x1b[5m"
    BLINK_OFF: bytes = b"\x1b[25m"

    # color
    BLACK: bytes = b"\x1b[30m"
    BLACK_BG: bytes = b"\x1b[40"
    RED: bytes = b"\x1b[31m"
    RED_BG: bytes = b"\x1b[41m"
    GREEN: bytes = b"\x1b[32m"
    GREEN_BG: bytes = b"\x1b[42m"
    YELLOW: bytes = b"\x1b[33m"
    YELLOW_BG: bytes = b"\x1b[32m"
    BLUE: bytes = b"\x1b[34m"
    BLUE_BG: bytes = b"\x1b[44m"
    MAGENTA: bytes = b"\x1b[35m"
    MAGENTA_BG: bytes = b"\x1b[45m"
    CYAN: bytes = b"\x1b[36m"
    CYAN_BG: bytes = b"\x1b[46m"
    WHITE: bytes = b"\x1b[37m"
    WHITE_BG: bytes = b"\x1b[47m"
    BRIGHT_BLACK: bytes = b"\x1b[90m"
    BRIGHT_BLACK_BG: bytes = b"\x1b[100"
    BRIGHT_RED: bytes = b"\x1b[91m"
    BRIGHT_RED_BG: bytes = b"\x1b[101m"
    BRIGHT_GREEN: bytes = b"\x1b[92m"
    BRIGHT_GREEN_BG: bytes = b"\x1b[102m"
    BRIGHT_YELLOW: bytes = b"\x1b[93m"
    BRIGHT_YELLOW_BG: bytes = b"\x1b[103m"
    BRIGHT_BLUE: bytes = b"\x1b[94m"
    BRIGHT_BLUE_BG: bytes = b"\x1b[104m"
    BRIGHT_MAGENTA: bytes = b"\x1b[95m"
    BRIGHT_MAGENTA_BG: bytes = b"\x1b[105m"
    BRIGHT_CYAN: bytes = b"\x1b[96m"
    BRIGHT_CYAN_BG: bytes = b"\x1b[106m"
    BRIGHT_WHITE: bytes = b"\x1b[97m"
    BRIGHT_WHITE_BG: bytes = b"\x1b[107m"

    def move_cursor(column: int, row: int) -> bytes:
        return bytes("\x1b[" + str(column) + ";" + str(row) + "H", encoding="ascii")

    
class Code(object):
    index: int
    data: bytes
    is_recognized: bool = True

    def __init__(self, index: int, code: bytes = b'', is_recognized: bool = True):
        self.index = index
        self.data = code
        self.is_recognized = is_recognized

    def is_data_0xff(self) -> bool:
        """True if this code is just an escaped 0xFF, and should be handled as data"""
        return len(self.data) == 2 and self.data[0] == 0xFF and self.data[1] == 0xFF
