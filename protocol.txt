  HEX   SYMBOL                      Description
####################################################################################################################
  00    NULL                        No operation
  07    BELL                        makes a sound or flashes the screen
  08    Back Space (BS)             erases last written character
  09    Horizontal Tab (HT)         moves a bit to the right ("tab")
  0A    Line Feed (LF)              goto next line, and stays on the same horizontal position
  0B    Vertical Tab (VT)           moves a couple of lines down, and moves the cursor to the leftmost position
  0C    Form Feed (FF)              blanks the screen (move printer to next paper)
  0D    Carriage Return (CR)        moves the cursor to the leftmost position
  81    Break (BRK)                 the BREAK key
  FF    Interpret as Command (IAC)  meaning that the following data is to be treated as a Telnet command

  The characters 0x20 to 0x7E are also defined (i.e. " " to "~")
  Newlines should essentially always be 0x0D 0x0A ("\r\n")
  The sequence FF FF should escape and be interpreted as the data FF

  HEX   COMMAND                     Example         Meaning
####################################################################################################################
  F0    SE                          FF F0           End of subnegotiation parameters
  F1    NOP                         FF F1           This sequence should just be ignored
  F2    Data Mark                   FF F2           TCP Urgent data that is sent with Synch
  F3    Break                       FF F3           The character BREAK (on the server-side)??
  F4    Interrupt Process (IP)      FF F4           Terminate current task or process
  F5    Abort Process (AO)          FF F5           Skip current output (used to break out of long data dumps)
  F6    Are You There (AYT)         FF F6           Ping other terminal, check if still alive
  F7    Erase Character (EC)        FF F7           Erase one 'print position' (may be multiple bytes)
  F8    Erase Line (EL)             FF F8           Erase current line
  F9    Go Ahead (GA)               FF F9           Tell other end to please maybe start sending data
  FA    Subnegotiation (SB)                         See below
  FB    WILL                        FF FA FB        "I offer to use these options"
  FC    WON'T                       FF FA FC        "I do not intend to use these options"
  FD    DO                          FF FA FD        "Please use these options"
  FE    DON'T                       FF FA FE        "Please do not start using these options"
  FF    Treat as data FF            FF FF           Escape sequence, interpret as raw data FF