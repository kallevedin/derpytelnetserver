from terminal import TelnetTerminal
from protocol import ESC, ansi

class TelnetService:
    banner = "################################\r\n" + \
             "#      Derpy Telnet Server     #\r\n" + \
             "################################\r\n"

    def __init__(self):
        return

    def initHandshake(self, terminal: TelnetTerminal):
        print("Sending init sequence to client ", + terminal.clientId, "\n")
        terminal.write(ansi.HOME + ansi.BRIGHT_WHITE_BG + ansi.RED)
        terminal.write(self.banner)
        terminal.write(ansi.NORMAL)
        # terminal.write(ansi.move_cursor(column=0, row=0))
        terminal.write(ESC.IAC_DO_TERMINAL_TYPE)


        terminal.write("HELLO :D :D\r\n")

        return

    def handleInput(self, data: bytes, terminal: TelnetTerminal):
        hex_str = ' '.join(format(x, '02x') for x in data)
        print("client wrote:", hex_str)

        if len(data) == 1 and data[0] == 0x03:  # user pressed CTRL+C
            terminal.close()
            return

        terminal.write("You wrote: " + data.decode(terminal.encoding))

        return
